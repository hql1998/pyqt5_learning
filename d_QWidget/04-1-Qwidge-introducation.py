from PyQt5.Qt import *
import sys


app = QApplication(sys.argv)

window = QWidget()
print("the MRO of Widge", QWidget.mro())
print("the bases of Widge", QWidget.__bases__)


window.resize(QSize(500, 500))
window.move(250, 250)

print("pos(),x,y", window.pos().x(),window.pos().y())
print("Geometry", window.geometry())
print("frameSize", window.frameSize())
print("frameGeometry", window.frameGeometry())


window.show()

print("-"*20)

print("pos(),x,y", window.pos().x(),window.pos().y())
print("Geometry", window.geometry())
print("frameSize", window.frameSize())
print("frameGeometry", window.frameGeometry())

sys.exit(app.exec_())