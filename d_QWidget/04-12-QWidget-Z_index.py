from PyQt5.Qt import *
import sys
import random as rd
sys.path.append("..")
from a_constant.constant_appearance import *



class Label(QLabel):
    def __init__(self, *args, **kwargs):
        #settting the moveFlage is to avoid the situation that mouse is setting setMouseTracking(True)

        super().__init__(*args, **kwargs)
        self.moveFlag = False

    def mousePressEvent(self, e):
        if e.button() == Qt.LeftButton:
            self.moveFlag = True
            self.lab_init_x = self.x()
            self.lab_init_y = self.y()

            self.mou_init_x = e.globalX()
            self.mou_init_y = e.globalY()

        e.ignore()

    def mouseMoveEvent(self, e):
        if self.moveFlag:
            vec_x = e.globalX() - self.mou_init_x
            vec_y = e.globalY() - self.mou_init_y
            self.move(self.lab_init_x + vec_x, self.lab_init_y + vec_y)

    def mouseReleaseEvent(self, ev):
        self.moveFlag = False


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)
        self.setWindowOpacity(0.9)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        number = 100
        label_w = 50
        label_h = 50
        label_m = (label_w - 28) * 0.5
        for i in range(number):
            label = Label(self)

            pixmap = QPixmap(r"E:\360极速浏览器下载\cursor.png").scaled(50, 50)
            label.setPixmap(pixmap)
            # label.setText(str(i))
            label.resize(label_w, label_h)
            x_ran = rd.randrange(0, iniWindowWidth-label_w)
            y_ran = rd.randrange(0, iniWindowHeight-label_h)
            label.move(x_ran,y_ran)
            label.setContentsMargins(label_m, 0, 0, 0)

            label.setStyleSheet("background-color:teal;border:2 solid white;font-size:16px")

        return None

    def mousePressEvent(self, evt):

        label = self.childAt(evt.pos())
        if label is not None:
            label.raise_()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())