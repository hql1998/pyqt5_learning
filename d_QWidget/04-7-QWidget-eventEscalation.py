from PyQt5.Qt import *
import sys


class MyWindow(QWidget):

    def mousePressEvent(self, evt):
        print(self.objectName()," clicked")
        print("evt.isAccepted(): ", evt.isAccepted(),end = "\n\n")


class MidWidget(QWidget):

    def mousePressEvent(self,evt):
        print(self.objectName(), " clicked")
        print("evt.isAccepted(): ", evt.isAccepted(),end = "\n\n")


class Label(QLabel):

    def mousePressEvent(self, evt):
        print(self.objectName(), " clicked")
        evt.ignore()
        print("evt.isAccepted(): ", evt.isAccepted(),end = "\n\n")


def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)


app = QApplication(sys.argv)

window = MyWindow()
window_title = "eventEscalation"
window.setObjectName("main window: ")
initial(window,window_title, 500, 500)

midWindow =MidWidget(window)
midWindow.setObjectName("Mid-Window: ")
midWindow.setAttribute(Qt.WA_StyledBackground, True)
midWindow.setStyleSheet("background-color:teal")
midWindow.setGeometry(150, 150, 200, 200)

label = Label(midWindow)
label.setObjectName("Label: ")
label.setText("click me")
label.setStyleSheet("background-color:blue;color:white;font-size:20px")

window.show()

sys.exit(app.exec_())