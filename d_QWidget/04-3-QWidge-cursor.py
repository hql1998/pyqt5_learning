from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Timer(QObject):

    def timerEvent(self, e):

        if self.counting < self.ending:
            print("ticked",self.counting)
            self.counting += 1

            cursor = self.cursor
            print(cursor.pos())
        else:
            self.killTimer(self.timer_id)


    def start(self,cursor,interval,ending = 50):

        self.counting = 0
        self.timer_id = self.startTimer(interval, Qt.PreciseTimer)
        # print(self.timer_id)
        self.ending = ending
        self.cursor = cursor



class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()

        self.setMinimumSize(400, 300)
        self.setFixedSize(iniWindowWidth, iniWindowHeight)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)
        self.setCursor(Qt.WhatsThisCursor)

        self.setup_ui(iniWindowWidth, iniWindowHeight)
        self.get_cursor_obj()


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = QLabel(self)
        label.setText("classification")
        label.resize(200, 100)
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        label.setStyleSheet("background-color:teal; color:white")

        # the setContentMargins() method could change the margin
        label.setContentsMargins(50, 20, 20, 20)
        print(label.contentsRect())

        # chnage the cursor style
        label.setCursor(Qt.OpenHandCursor)

        # self defined style cursor
        pixmap = QPixmap(r"E:\360极速浏览器下载\cursor.png")
        pixmap_scaled = pixmap.scaled(50, 50)
        cursor = QCursor(pixmap_scaled, 5, 5)
        label.setCursor(cursor)

        cursor_current =  label.cursor()
        print("cursor_current.pos() ", cursor_current.pos())
        print(cursor_current.mask())

        label.unsetCursor()

        return None


    def get_cursor_obj(self):
        cursor = self.cursor()
        timer = Timer(self)
        # timer.start(cursor,100, 300)

        # self.mouseMoveEvent()


        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())