"""
this program is designed to show an exercise
of window moving along with mouse movement when
click on client area
"""
from PyQt5.Qt import *
import sys


class MyWindow(QWidget):

    def __init__(self, *args, **kwargs):
        #settting the moveFlage is to avoid the situation that mouse is setting window.setMouseTracking(True)

        super(MyWindow, self).__init__(*args, **kwargs)
        self.moveFlag = False


    def mousePressEvent(self, e):

        if e.button() == Qt.LeftButton:
            self.moveFlag = True

            self.win_init_x = self.x()
            self.win_init_y = self.y()

            self.mou_init_x = e.globalX()
            self.mou_init_y = e.globalY()


    def mouseMoveEvent(self, e):

        if self.moveFlag:
            vec_x = e.globalX() - self.mou_init_x
            vec_y = e.globalY() - self.mou_init_y
            self.move(self.win_init_x + vec_x, self.win_init_y + vec_y)

    def mouseReleaseEvent(self, e):
        self.moveFlag = False

def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)


app = QApplication(sys.argv)

window = MyWindow()
window_title = "windowMovement"
window.setObjectName("main window: ")
initial(window,window_title, 500, 500)

window.setMouseTracking(True)


window.show()

sys.exit(app.exec_())