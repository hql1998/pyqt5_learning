from PyQt5.Qt import *
import sys

class MyWidget(QWidget):

    def mouseMoveEvent(self, me):
        print(self.objectName(),"globalPos()", me.globalPos())
        print(self.objectName(), "localPos()", me.localPos())
        print(self.objectName(), "pos()", me.pos())
        print(self.objectName(), "windowPos()", me.windowPos())
        print("*"*20)


def initial(window, width, height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width, height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)


app = QApplication(sys.argv)

window = MyWidget()
window.setObjectName("main window: ")
initial(window, 500, 500)
window.setMouseTracking(True)
print("window.hasMouseTracking", window.hasMouseTracking())


window.show()

sys.exit(app.exec_())

# the outputs:
#
# window.hasMouseTracking True
# main window:  globalPos() PyQt5.QtCore.QPoint(1199, 463)
# main window:  localPos() PyQt5.QtCore.QPointF(488.0, 142.0)
# main window:  pos() PyQt5.QtCore.QPoint(488, 142)
# main window:  windowPos() PyQt5.QtCore.QPointF(488.0, 142.0)
# ********************