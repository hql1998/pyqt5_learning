import sys,math
from PyQt5.Qt import *


app = QApplication(sys.argv)

window = QWidget()
window.show()

window_width = 500
window_height = 400

number = 100
column_number = 8
row_number = math.ceil(number / column_number)

window.resize(window_width, window_height)

widget_width = window_width / column_number
widget_height = window_height / row_number


for i in range(number):
    gadget_x = i % column_number * widget_width
    gadget_y = i // column_number * widget_height

    gadget = QWidget(window)
    gadget.resize(widget_width,widget_height)
    gadget.move(gadget_x, gadget_y)

    gadget.setStyleSheet("background-color:teal;")

    gadget.show()



sys.exit(app.exec_())