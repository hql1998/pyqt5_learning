from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *



class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)

    def loginCheck(self):

        le = self.findChild(QLineEdit)
        text = le.text()
        if text == "HQL":
            self.findChild(QLabel).setVisible(True)

    def handlele(self,e):

        if e != "":
            btn = self.findChild(QPushButton)
            btn.setEnabled(True)



    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = QLabel(self)
        label.setText("classification")
        label.resize(100, 50)
        label.move((iniWindowWidth - 100) / 2, 30)
        label.setStyleSheet("font-size:20px")
        label.setVisible(False)

        le = QLineEdit(self)
        le.setText("input the password")
        le.move((iniWindowWidth - 100) / 2, 90)
        le.textChanged.connect(self.handlele)

        btn = QPushButton(self)
        btn.setText("login")
        btn.pressed.connect(self.loginCheck)
        btn.setEnabled(False)
        btn.move((iniWindowWidth - 100) / 2, 130)

        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()

    win2 = QMainWindow()
    win2.statusBar()
    win2.setWindowTitle("edited[*]")
    win2.setWindowModified(True)
    win2.menuBar()
    win2.setWindowFlags(Qt.WindowContextHelpButtonHint)
    label1 = QLabel(win2)
    label1.setText("a label")
    label1.setStatusTip("ok?")
    label1.setToolTip("ok yeah ??")
    label1.setWhatsThis("it's a good thing !")
    win2.show()

    sys.exit(app.exec_())

