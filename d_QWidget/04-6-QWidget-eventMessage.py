from PyQt5.Qt import *
from PyQt5 import QtCore
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = QLabel(self)
        label.setText("classification")
        label.resize(100, 50)
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)
        label.setWindowOpacity(1)

        return None

    def showEvent(self, se):
        print(self.windowTitle()," showEvent ",se)

    def closeEvent(self, ce):
        print(self.windowTitle()," closeEvent ", ce)

    def moveEvent(self,me):
        print(self.windowTitle()," moveEvent ", me)

    def resizeEvent(self, re):
        print(self.windowTitle(), " resizeEvent ", re)

    def enterEvent(self, e) :
        self.setStyleSheet("background-color:rgba(125,123,156,0.8)")
        # self.setWindowOpacity(0.5)
        # self.setWindowFlags(FramelessWindowHint())
        # self.setAttribute(WA_TranslucentBackground())
        # self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

    def leaveEvent(self, e) :
        self.setStyleSheet("background-color:rgba(125,123,156,0.5)")
        self.setWindowOpacity(1)

    # def keyPressEvent(self, a0: QtGui.QKeyEvent) -> None:
    # def keyReleaseEvent(self, a0: QtGui.QKeyEvent) -> None:

    # def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
    # def mouseDoubleClickEvent(self, a0: QtGui.QMouseEvent) -> None:
    # def mouseReleaseEvent(self, a0: QtGui.QMouseEvent) -> None:

    # def focusInEvent(self, a0: QtGui.QFocusEvent) -> None:
    # def focusOutEvent(self, a0: QtGui.QFocusEvent) -> None:

    # def paintEvent(self, a0: QtGui.QPaintEvent) -> None:

    # def inputMethodEvent(self, a0: QtGui.QInputMethodEvent) -> None:

    # def changeEvent(self, a0: QtCore.QEvent) -> None:

    # def dropEvent(self, a0: QtGui.QDropEvent) -> None:
    # def dragEnterEvent(self, a0: QtGui.QDragEnterEvent) -> None:
    # def dragLeaveEvent(self, a0: QtGui.QDragLeaveEvent) -> None:
    # def dragMoveEvent(self, a0: QtGui.QDragMoveEvent) -> None:

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())