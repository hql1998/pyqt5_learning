from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class MyLabel(QLabel):

    def enterEvent(self, e):
        self.setStyleSheet("background-color:red")

    def leaveEvent(self, e):
        self.setStyleSheet("background-color:silver")


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.setWindowOpacity(0.8)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)
        self.margin = 10
        self.moveFlag = False

        self.setup_ui()

    def setup_ui(self):

        windowWidth = self.size().width()

        self.label_close = self.make_label("close.png", 1, windowWidth)
        self.label_close.setObjectName("close_button")

        self.label_max = self.make_label("Maximize.png", 2, windowWidth)
        self.label_max.setObjectName("max_button")

        self.label_min = self.make_label("Minimize.png", 3, windowWidth)
        self.label_min.setObjectName("min_button")

        return None

    def make_label(self, path_str, index, WindowWidth):
        self.label_w = 25
        self.label_h = 25

        label = MyLabel(self)
        pximap = QPixmap(path_str).scaled(self.label_w, self.label_h)
        label.setPixmap(pximap)
        label.adjustSize()
        label.setStyleSheet("background-color:silver")

        label.move(WindowWidth - self.label_w * index - (index-1) * self.label_w * 0.1 - self.margin, self.margin)
        return label

    def mousePressEvent(self, e):
        window_state = self.windowState()
        gadget = self.childAt(e.pos())
        if gadget is not None:
            if gadget.objectName() == "close_button":
                self.close()
            elif gadget.objectName() == "max_button":
                self.setWindowState(Qt.WindowMaximized)
                gadget.setObjectName("restore_button")
                gadget.setPixmap(QPixmap("restore.png").scaled(self.label_w,self.label_h))

            elif gadget.objectName() == "min_button":
                self.setWindowState(Qt.WindowMinimized)

            elif gadget.objectName() == "restore_button":
                self.setWindowState(Qt.WindowNoState)
                gadget.setObjectName("max_button")
                gadget.setPixmap(QPixmap("Maximize.png").scaled(self.label_w, self.label_h))

        if e.button() == Qt.LeftButton:
            self.moveFlag = True
            self.mou_init_x = e.globalX()
            self.mou_init_y = e.globalY()
            self.win_init_x = self.x()
            self.win_init_y = self.y()

    def mouseReleaseEvent(self, e):
        self.moveFlag = False


    def mouseMoveEvent(self, e):
        if self.moveFlag:
            vec_x = e.globalX() - self.mou_init_x
            vec_y = e.globalY() - self.mou_init_y
            self.move(self.win_init_x+vec_x, self.win_init_y+vec_y)

    def resizeEvent(self, e):

        window_width = e.size().width()

        self.label_close.move(window_width - self.label_w * 1 - self.margin, self.margin)
        self.label_max.move(window_width - self.label_w * 2 - self.label_w * 0.1 - self.margin, self.margin)
        self.label_min.move(window_width - self.label_w * 3 - 2 * self.label_w * 0.1 - self.margin, self.margin)




if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())