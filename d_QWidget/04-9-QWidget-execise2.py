from PyQt5.Qt import *
import sys


def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)


class MyLineEdit(QLineEdit):
    def keyPressEvent(self, e):

        # e -> QKeyEvent
        if e.key() == Qt.Key_Tab:
            self.setText("you entered a tab key")
        elif e.key() == Qt.Key_S and e.modifiers() == Qt.ControlModifier:
            self.setText("you entered S + Ctrl")
        elif e.key() == Qt.Key_A and e.modifiers() == Qt.AltModifier | Qt.ShiftModifier:
            self.setText("you entered Alt + Shit + A")
        self.adjustSize()

app = QApplication(sys.argv)

window = QWidget()
window_title = "listenToKeyboardInputs"
window.setObjectName("main window: ")
initial(window,window_title, 500, 500)

le = MyLineEdit(window)
le.setGeometry(200,230,100,40)
le.setFocus()
# le.grabKeyboard()

window.show()

sys.exit(app.exec_())