from PyQt5.Qt import *
import sys

class MyWidget(QWidget):

    def mouseMoveEvent(self, me):
        mouse_pos = me.localPos()
        print(mouse_pos)
        label = self.findChild(QLabel, "1stlabel")
        print(label)
        label.move(mouse_pos.x(),mouse_pos.y())

def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)

    pixmap = QPixmap(r"E:\360极速浏览器下载\cursor.png")
    pixmap_scaled = pixmap.scaled(40,40)
    cursor = QCursor(pixmap_scaled,0,5)
    window.setCursor(cursor)


app = QApplication(sys.argv)

window = MyWidget()
window_title = "cursorMove"
window.setObjectName("main window: ")
initial(window,window_title, 500, 500)

label = QLabel(window)
label.setText("HQL's Classification")
label.setObjectName("1stlabel")
label.adjustSize()
margin = 10
label_width = label.size().width()
label.resize(label_width+margin*2,50)
label.setContentsMargins(margin,0,0,0)
label.setStyleSheet("background-color:teal;color:white")


window.show()

sys.exit(app.exec_())