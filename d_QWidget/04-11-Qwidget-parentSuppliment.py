import sys,math
from PyQt5.Qt import *

class Mywindow(QWidget):

    def __init__(self, *argv, **kwargs):

        super().__init__(*argv, **kwargs)
        self.setup_ui()


    def setup_ui(self):
        window_width = 500
        window_height = 400
        number = 104
        column_number = 8

        row_number = math.ceil(number / column_number)

        self.resize(window_width, window_height)

        widget_width = window_width / column_number
        widget_height = window_height / row_number

        for i in range(number):
            gadget_x = i % column_number * widget_width
            gadget_y = i // column_number * widget_height

            gadget = QWidget(self)

            gadget.resize(widget_width, widget_height)
            gadget.move(gadget_x, gadget_y)
            gadget.color = "teal"
            gadget.setStyleSheet("background-color:teal;")

        return None


    def mousePressEvent(self, e):

        if e.button() == Qt.LeftButton:
            gadget = self.childAt(e.pos())

            if gadget is not None:
                if gadget.color == "teal":
                    gadget.setStyleSheet("background-color: cyan")
                    gadget.color = "cyan"
                else:
                    gadget.setStyleSheet("background-color: teal")
                    gadget.color = "teal"



app = QApplication(sys.argv)

window = Mywindow()

window.show()







sys.exit(app.exec_())