from PyQt5.Qt import *
import sys

"""
this file is designed to show enter and leave Event of mouse
when entering the window, the label shows welcome here
while leaving the window, the label shows thank you for your coming!
"""

def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)

class MyWindow(QWidget):

    def enterEvent(self, evt):
        label = self.findChild(QLabel)
        label.setText("welcome here!")
        label.adjustSize()

    def leaveEvent(self,evt):
        label = self.findChild(QLabel)
        label.setText("thank you for your coming!")
        label.adjustSize()

app = QApplication(sys.argv)

window = MyWindow()
window_title = "mouseInAndOut"
window.setObjectName("main window: ")
initial(window, window_title, 500, 500)

label = QLabel(window)
label.setStyleSheet("font-size:20px;")
label.move(230,230)

window.show()

sys.exit(app.exec_())