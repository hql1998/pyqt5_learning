from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *

class MyLabel(QLabel):

    def timerEvent(self, event) :
        current_label = int(self.text())
        current_label -= 1
        self.setText(str(current_label))

        if current_label == 0:
            self.killTimer(self.timer_id)

    def start(self,ms,begin):
        self.setText(str(begin))
        self.timer_id = self.startTimer(ms,Qt.PreciseTimer)




class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):

        with open("03-1-QObject-style.qss") as f:
            qApp.setStyleSheet(f.read())

        label = MyLabel(self)
        label.start(1000,50)
        label.resize(100, 50)
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())