from PyQt5.Qt import *
import sys


class App(QApplication):

    def notify(self, obj, event):
        if obj.inherits("QPushButton") and event.type() == QEvent.MouseButtonPress:
            print("notify processing ",obj, event)

        return super().notify(obj, event)


class Button(QPushButton):

    # this method will distribute all the events to correspoding event handler(function)
    # like MoseButtonPress event coms from app.notify, and it will be send to such Button's event method
    # and event method will send the MouseButtonPress event to MouseButtonPress() method(event handler of the button)
    def event(self, e):
        if e.type() == QEvent.MouseButtonPress:
            print("recever's event function processing ",e)
        return super().event(e)

    # this function is specific to deal with mouse press event from button event function
    # it will triger the clicked signal emission of button.
    def mousePressEvent(self, e):
        print("mousePressEvent tragered")
        return super().mousePressEvent(e)

    def mouseDoubleClickEvent(self, e):
        print("mouseDoubleClickEvent tragered")
        return super().mousePressEvent(e)


app = App(sys.argv)
window = QWidget()

btn = Button(window)
btn.setText("button")
btn.move(110,120)
btn.clicked.connect(lambda x: print("signal-slot: button was clicked ",x))

window.show()
sys.exit(app.exec_())