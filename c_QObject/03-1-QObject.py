from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)
        self.QObject_parent()
        # self.QObject_name_attribute()
        # self.QObject_signal_API()

    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = QLabel(self)
        label.setText("classification")
        label.resize(100, 50)
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        return None

    def QObject_parent_linkage(self):
        mro = QObject.mro()
        for i in mro:
            print(i)
        return None

    def QObject_name_attribute(self):

        # ****************test Api********************
        # obj = QObject()
        # obj.setObjectName("object_Qt")
        # print(obj.objectName())
        #
        # obj.setProperty("notice_level_1", "error")
        # obj.setProperty("notice_level_2", "warning")
        # print(obj.dynamicPropertyNames())
        # ****************test Api********************

        # ****************h-QSS example********************
        with open("03-1-QObject-style.qss","r") as f:
            qApp.setStyleSheet(f.read())

        label1 = QLabel(self)
        label1.setText("h-QSS introduction")

        label2 = QLabel(self)
        label2.setText("h-QSS introduction")
        label2.move(0, 25)

        label3 = QLabel(self)
        label3.setText("h-QSS introduction")
        label3.move(0, 50)

        label4 = QLabel(self)
        label4.setText("h-QSS introduction")
        label4.move(0, 75)
        label4.setObjectName("message")

        btn1 = QPushButton(self)
        btn1.setText("AButton")
        btn1.setObjectName("message")
        btn1.move(0,100)


        # set qss string
        label1.setStyleSheet("font-size:20px; color:red")

        for i in self.children():
            print(i)
            if i.inherits("QLabel"):
                i.setStyleSheet("font-size:20px; color:teal")

        # ****************h-QSS example********************




        return None

    def QObject_parent(self):
        # ****************test API********************
        # obj1 = QObject()
        # obj2 = QObject()
        #
        # obj1.setParent(obj2)
        #
        # print(repr(obj1.parent()))
        # obj2.children()
        # obj2.findChild()
        # obj2.findChildren()
        # ****************test API********************

        # ****************memorry manage********************
        obj1 = QObject()
        obj2 = QObject()
        self.obj1 = obj1
        obj2.setParent(obj1)

        # linsten to the release event of obj2
        obj1.destroyed.connect(lambda: print("obj1 is released"))
        obj2.destroyed.connect(lambda: print("obj2 is released"))

        # delete an object, you can't use del keyword
        # rather you need use deleteLater method to release the link
        # of the parentship between obj1 and obj2

        # del obj2
        obj2.deleteLater()
        # ****************memorry manage********************

    def QObject_signal_API(self):
        # ****************signal_API********************
        # def delobj():
        #     del self.obj
        #
        # obj = QObject()
        # self.obj = obj
        #
        # label = QPushButton()
        # label.setParent(self)
        # label.setText("A Text")
        # label.clicked.connect(delobj)
        #
        # # obj has two built-in signal, i.e. objectNameChanged(Name),destroyed(obj), within the parentheses
        # # they are the augument could be passed into and used by the connected slot function
        # obj.destroyed.connect(lambda x: print("the obej is destroy",x))
        # obj.destroyed.connect(lambda x: print("the obej is destroy2",x))
        # obj.destroyed.connect(lambda x: print("the obej is destroy",x))
        # obj.destroyed.connect(lambda x: print("the obej is destroy2",x))
        #
        # obj.objectNameChanged.connect(lambda x: print("the obej name is changed",x))
        #
        # # temporarily blocked all the signals
        # obj.blockSignals(True)
        # obj.setObjectName("test_the_change_name_api")
        # print("test some obj's signaling is blocked or not",obj.signalsBlocked())
        # # recover the signaling
        # obj.blockSignals(False)
        #
        # # count the number of the receivers of a signal
        # print("number of receivers", obj.receivers(obj.destroyed))
        #
        # # forbide all the connections
        # # obj.disconnect()
        #
        # obj.setObjectName("test_the_change_name_api2")
        # ****************signal_API********************

        # ****************signal_example********************
        def add_name(title):
            self.blockSignals(True)
            self.setWindowTitle("HQL's "+title)
            self.blockSignals(False)


        self.windowTitleChanged.connect(add_name)

        self.setWindowTitle("Software")

        self.setWindowTitle("Practice")
        # ****************signal_example********************

if __name__ == "__main__":
    app = QApplication(sys.argv)


    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()

    # ****************parent_example2********************
    # win1 = QWidget()
    # win1.setWindowTitle("win1")
    # win1.show()
    #
    # win2 = QWidget()
    # win2.setWindowTitle("win2")
    # win2.setStyleSheet("background-color: rgba(128,12,12,0.7);")
    # win2.setParent(win1)
    #
    # win2.resize(1000, 1000)
    # ****************parent_example2********************

    # ****************parent_example2********************
    # win_root = QWidget()
    #
    # label1 = QLabel()
    # label1.setText("label1")
    # label1.setParent(win_root)
    #
    # label2 = QLabel(win_root)
    # label2.setText("label2")
    # label2.move(0, 30)
    #
    # btn = QPushButton(win_root)
    # btn.setText("button1")
    # btn.move(0, 60)
    #
    # for i in win_root.findChildren(QLabel):
    #     i.setStyleSheet("background-color: cyan")
    # ****************parent_example2********************

    # win_root.show()



    sys.exit(app.exec_())