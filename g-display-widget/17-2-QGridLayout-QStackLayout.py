from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        labels = []
        for i in range(16):
            label = QLabel("label " + str(i + 1))
            label.setStyleSheet(
                "background-color:rgb(" + str(i * 10) + "," + str(i * 12) + "," + str(i * 13) + ")" + ";color:white")
            label.setAlignment(Qt.AlignCenter)
            labels.append(label)

        gl = QGridLayout()
        gl2 = QGridLayout()
        sl = QStackedLayout()

        self.setLayout(sl)

        for i in range(8):

            gl.addWidget(labels[i],0,i)
            gl2.addWidget(labels[i+8], i, 0)

        # gl.setSpacing(20)
        gl2.setSpacing(20)
        gl2.setContentsMargins(20,20,20,20)

        win1 = QWidget(self)
        win1.setLayout(gl)

        win2 = QWidget(self)
        win2.setLayout(gl2)

        # Properties are quite different
        win2.ok = "good!"
        win2.setProperty("nihao","yes")
        print(win2.__dict__)
        print(win2.property("nihao"))
        print(win2.property("ok"))


        sl.addWidget(win1)
        sl.addWidget(win2)
        # sl.setStackingMode(QStackedLayout.StackAll)

        sl.setCurrentIndex(1)


        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())