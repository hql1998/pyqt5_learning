from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):

        labels = []
        for i in range(16):
            label = QLabel("label " + str(i + 1))
            label.setStyleSheet("background-color:rgb("+str(i*10)+","+str(i*12)+","+str(i*13)+")" + ";color:white")
            label.setAlignment(Qt.AlignCenter)
            labels.append(label)

        boxLayout = QBoxLayout(QBoxLayout.LeftToRight)
        sub_boxLayout = QBoxLayout(QBoxLayout.TopToBottom)

        for i in range(8):

            boxLayout.addWidget(labels[i],1)
            sub_boxLayout.addWidget(labels[i+8],1)

        self.setLayout(boxLayout)

        # boxLayout.insertStretch(4,1)
        boxLayout.insertLayout(5,sub_boxLayout,6)
        # boxLayout.insertStretch(6, 1)


        formLayout = QFormLayout()
        formLayout.addRow(QLabel("Name"), QLineEdit("input your name"))
        formLayout.addRow(QLabel("Student Number"), QLineEdit("input your name"))
        formLayout.addRow(QLabel("Home Address"), QLineEdit("input your name"))

        boxLayout.insertLayout(6,formLayout,2)





        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())