from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = QLabel(self)
        label.setText("classification")
        label.resize(100, 50)
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        pb = QProgressBar(self)
        pb.resize(300,30)
        pb.move(30, 30)
        pb.setValue(30)

        pd = QProgressDialog("hint_txt", "cencel_text", 0, 100, self)
        pd.open()

        timer = QTimer(pb)
        timer.setInterval(100)

        def pb_plus():

            if pb.value() == pb.maximum():
                timer.stop()
            pb.setValue(pb.value()+1)
            pd.setValue(pd.value() + 1)
            print("timer tick tuck")

        timer.timeout.connect(pb_plus)
        timer.start()

        em = QErrorMessage(self)
        em.setWindowModality(Qt.WindowModal) # this works
        em.showMessage("this a piece of error message")
        em.setWindowTitle("Error!")
        # em.setModal(True) # not work


        QErrorMessage.qtHandler()
        qDebug("this is a debug message")



        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())