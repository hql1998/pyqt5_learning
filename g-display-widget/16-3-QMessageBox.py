from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):

        mb = QMessageBox(self)
        mb.setWindowTitle("this is a message box")
        mb.setText("<h1>this is a h1 text</h1>")
        mb.setInformativeText("<h2>this is informative h2 text</h2>")
        mb.setIcon(QMessageBox.Warning)
        mb.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        mb.setDetailedText("this is a detailed text box")
        mb.open()

        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())