from resource.login_ui import Ui_Form
from PyQt5.Qt import QWidget,pyqtSignal,Qt,QPropertyAnimation,QAbstractAnimation,QUrl,QDesktopServices,QPoint
import resource.img_rc

class Login_Panel(QWidget, Ui_Form):
    login_clicked_signal = pyqtSignal(str,str)
    go_signup_clicked_signal = pyqtSignal()

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.setAttribute(Qt.WA_StyledBackground, True)
        # self.setWindowFlags(Qt.FramelessWindowHint)
        self.setupUi(self)
        # self.setContentsMargins(10, 10, 10, 10)
        self.user_name_cb.addItems(["1873964004"])


    def format_check(self):
        user_name = self.user_name_cb.currentText()
        pwd = self.pwd_le.text()
        if len(user_name) >0 and len(pwd) > 0:
            self.login_btn.setEnabled(True)
        else:
            self.login_btn.setEnabled(False)

    def qrcode_clicked(self):
        addr = "http://wpa.qq.com/msgrd?v=3&uin=1873964004&site=qq&menu=yes"
        QDesktopServices.openUrl(QUrl(addr))

    def login_clicked(self):

        user_name = self.user_name_cb.currentText()
        pwd = self.pwd_le.text()
        self.login_clicked_signal.emit(user_name, pwd)


    def go_signup_clicked(self):
        self.go_signup_clicked_signal.emit()

    def auto_toggled(self, flag):
        if flag:
            self.rem_pwd_cb.setChecked(True)

    def rem_toggled(self, flag):
        if not flag:
            self.auto_login_cb.setChecked(False)

    def auto_login_handle(self,user_name, pwd):

        self.user_name_cb.setCurrentText(user_name)
        self.pwd_le.setText(pwd)
        self.login_clicked()

    def login_error(self):

        seq_ani = QPropertyAnimation(self.form_box, b"pos", self)
        seq_ani.setKeyValues([(0, self.form_box.pos()),
                             (0.25, self.form_box.pos() + QPoint(50, 0)),
                             (0.5, self.form_box.pos()),
                             (0.75, self.form_box.pos() - QPoint(50, 0)),
                             (0.1, self.form_box.pos())])
        seq_ani.setDuration(100)
        seq_ani.setLoopCount(3)
        seq_ani.start(QAbstractAnimation.DeleteWhenStopped)
