# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(600, 400)
        Form.setMinimumSize(QtCore.QSize(600, 400))
        Form.setMaximumSize(QtCore.QSize(600, 400))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/login/image/flower_bud.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setMaximumSize(QtCore.QSize(600, 200))
        self.widget.setStyleSheet("background-image: url(:/login/image/flower600_160.png);\n"
"background-repeat: no-repeat;\n"
"background-position:center;")
        self.widget.setObjectName("widget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setStyleSheet("font: 60pt \"Chiller\";\n"
"font-weight:900;\n"
"text-align:center;\n"
"color: rgb(255, 147, 70);")
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label, 0, QtCore.Qt.AlignHCenter)
        self.verticalLayout.addWidget(self.widget)
        self.form_box = QtWidgets.QWidget(Form)
        self.form_box.setObjectName("form_box")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.form_box)
        self.horizontalLayout.setContentsMargins(20, 20, 20, 20)
        self.horizontalLayout.setSpacing(20)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.go_signup_btn = QtWidgets.QPushButton(self.form_box)
        self.go_signup_btn.setStyleSheet("font: 16pt \"Chiller\";\n"
"font-wight:800;")
        self.go_signup_btn.setFlat(True)
        self.go_signup_btn.setObjectName("go_signup_btn")
        self.horizontalLayout.addWidget(self.go_signup_btn, 0, QtCore.Qt.AlignBottom)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.rem_pwd_cb = QtWidgets.QCheckBox(self.form_box)
        self.rem_pwd_cb.setStyleSheet("QCheckBox{\n"
"font: 16pt \"Chiller\";\n"
"}")
        self.rem_pwd_cb.setObjectName("rem_pwd_cb")
        self.gridLayout.addWidget(self.rem_pwd_cb, 2, 1, 1, 1, QtCore.Qt.AlignHCenter)
        self.user_name_cb = QtWidgets.QComboBox(self.form_box)
        self.user_name_cb.setMinimumSize(QtCore.QSize(250, 40))
        self.user_name_cb.setMaximumSize(QtCore.QSize(250, 40))
        self.user_name_cb.setStyleSheet("QComboBox{\n"
"background-color: rgba(255, 255, 255, 2);\n"
"border: 2px solid white;\n"
"border-top: None;\n"
"border-left: None;\n"
"border-right: None;\n"
"font: 75 18pt \"Adobe Devanagari\";\n"
"color: rgb(59, 39, 48);\n"
"}\n"
"QComboBox:hover{\n"
"border: 2px solid rgb(85, 255, 255);\n"
"border-top: None;\n"
"border-left: None;\n"
"border-right: None;\n"
"}\n"
"\n"
"QComboBox::drop-down{\n"
"background-image: url(:/login/image/down-arrow.png);\n"
"background-position:center;\n"
"}\n"
"\n"
"QComboBox:: drop-arrow{\n"
"background-color: black;\n"
"}\n"
"")
        self.user_name_cb.setEditable(True)
        self.user_name_cb.setFrame(False)
        self.user_name_cb.setObjectName("user_name_cb")
        self.gridLayout.addWidget(self.user_name_cb, 0, 0, 1, 2, QtCore.Qt.AlignHCenter)
        self.pwd_le = QtWidgets.QLineEdit(self.form_box)
        self.pwd_le.setMinimumSize(QtCore.QSize(250, 40))
        self.pwd_le.setMaximumSize(QtCore.QSize(250, 40))
        self.pwd_le.setStyleSheet("QLineEdit {\n"
"border:None;\n"
"border-bottom:2px solid white;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"font: 75 16pt \"Adobe Devanagari\";\n"
"color: rgb(59, 39, 48);\n"
"}\n"
"QLineEdit:hover{\n"
"border-bottom-color:rgb(85, 255, 255);\n"
"}")
        self.pwd_le.setEchoMode(QtWidgets.QLineEdit.Password)
        self.pwd_le.setObjectName("pwd_le")
        self.gridLayout.addWidget(self.pwd_le, 1, 0, 1, 2, QtCore.Qt.AlignHCenter)
        self.login_btn = QtWidgets.QPushButton(self.form_box)
        self.login_btn.setEnabled(False)
        self.login_btn.setMinimumSize(QtCore.QSize(250, 40))
        self.login_btn.setMaximumSize(QtCore.QSize(250, 40))
        self.login_btn.setStyleSheet("QPushButton{\n"
"font: 20pt \"Chiller\";\n"
"font-weight:900;\n"
"border-radius:20px;\n"
"color:rgb(65, 42, 48);\n"
"background-color: rgb(255, 147, 70);\n"
"}\n"
"QPushButton:hover {\n"
"background-color: rgb(255, 172, 55);\n"
"}\n"
"QPushButton:disabled {\n"
"background-color: rgb(160, 184, 179)\n"
"}")
        self.login_btn.setFlat(False)
        self.login_btn.setObjectName("login_btn")
        self.gridLayout.addWidget(self.login_btn, 3, 0, 1, 2, QtCore.Qt.AlignHCenter)
        self.auto_login_cb = QtWidgets.QCheckBox(self.form_box)
        self.auto_login_cb.setStyleSheet("QCheckBox{\n"
"font: 16pt \"Chiller\";\n"
"}")
        self.auto_login_cb.setObjectName("auto_login_cb")
        self.gridLayout.addWidget(self.auto_login_cb, 2, 0, 1, 1, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.horizontalLayout.addLayout(self.gridLayout)
        self.qrcode_btn = QtWidgets.QPushButton(self.form_box)
        self.qrcode_btn.setMinimumSize(QtCore.QSize(100, 100))
        self.qrcode_btn.setMaximumSize(QtCore.QSize(100, 10))
        self.qrcode_btn.setStyleSheet("border-image: url(:/login/image/qrcode.png);")
        self.qrcode_btn.setText("")
        self.qrcode_btn.setObjectName("qrcode_btn")
        self.horizontalLayout.addWidget(self.qrcode_btn)
        self.horizontalLayout.setStretch(1, 6)
        self.verticalLayout.addWidget(self.form_box)
        self.verticalLayout.setStretch(0, 2)
        self.verticalLayout.setStretch(1, 3)

        self.retranslateUi(Form)
        self.user_name_cb.currentTextChanged['QString'].connect(Form.format_check)
        self.pwd_le.textChanged['QString'].connect(Form.format_check)
        self.qrcode_btn.clicked.connect(Form.qrcode_clicked)
        self.login_btn.clicked.connect(Form.login_clicked)
        self.go_signup_btn.clicked.connect(Form.go_signup_clicked)
        self.auto_login_cb.toggled['bool'].connect(Form.auto_toggled)
        self.rem_pwd_cb.toggled['bool'].connect(Form.rem_toggled)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Login"))
        self.label.setText(_translate("Form", "Caculator"))
        self.go_signup_btn.setText(_translate("Form", "<- Sign Up"))
        self.rem_pwd_cb.setText(_translate("Form", "Remember password"))
        self.login_btn.setText(_translate("Form", "Login"))
        self.auto_login_cb.setText(_translate("Form", "Auto Login"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
