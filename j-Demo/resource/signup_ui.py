# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'signup.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(600, 400)
        Form.setMinimumSize(QtCore.QSize(600, 400))
        Form.setMaximumSize(QtCore.QSize(600, 400))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/signup/image/flower_ini.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        Form.setStyleSheet("background-image: url(:/signup/image/bg_600_400.png);")
        self.menu_btn = QtWidgets.QPushButton(Form)
        self.menu_btn.setGeometry(QtCore.QRect(9, 9, 60, 60))
        self.menu_btn.setMinimumSize(QtCore.QSize(60, 60))
        self.menu_btn.setMaximumSize(QtCore.QSize(60, 60))
        self.menu_btn.setStyleSheet("QPushButton{\n"
"font: 16pt \"Chiller\";\n"
"color: rgb(255, 255, 255);\n"
"border-radius:30px;\n"
"background-color: rgb(118, 115, 125);\n"
"\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"background-color: rgb(148, 145, 157);\n"
"}\n"
"QPushButton:checked{\n"
"background-color: rgb(63, 61, 67);\n"
"}\n"
"\n"
"")
        self.menu_btn.setCheckable(True)
        self.menu_btn.setObjectName("menu_btn")
        self.exit_btn = QtWidgets.QPushButton(Form)
        self.exit_btn.setGeometry(QtCore.QRect(9, 75, 60, 60))
        self.exit_btn.setMinimumSize(QtCore.QSize(60, 60))
        self.exit_btn.setMaximumSize(QtCore.QSize(60, 60))
        self.exit_btn.setStyleSheet("QPushButton{\n"
"font: 16pt \"Chiller\";\n"
"color: rgb(255, 255, 255);\n"
"border-radius:30px;\n"
"background-color: rgb(65, 62, 48);\n"
"}\n"
"QPushButton:hover{\n"
"background-color:rgb(97, 92, 72);\n"
"}")
        self.exit_btn.setObjectName("exit_btn")
        self.reset_btn = QtWidgets.QPushButton(Form)
        self.reset_btn.setGeometry(QtCore.QRect(75, 75, 60, 60))
        self.reset_btn.setMinimumSize(QtCore.QSize(60, 60))
        self.reset_btn.setMaximumSize(QtCore.QSize(60, 60))
        self.reset_btn.setStyleSheet("QPushButton{\n"
"font: 16pt \"Chiller\";\n"
"color: rgb(255, 255, 255);\n"
"border-radius:30px;\n"
"background-color: rgb(145, 106, 86);\n"
"}\n"
"QPushButton:hover{\n"
"background-color: rgb(172, 125, 102);\n"
"}")
        self.reset_btn.setObjectName("reset_btn")
        self.about_btn = QtWidgets.QPushButton(Form)
        self.about_btn.setGeometry(QtCore.QRect(75, 9, 60, 60))
        self.about_btn.setMinimumSize(QtCore.QSize(60, 60))
        self.about_btn.setMaximumSize(QtCore.QSize(60, 60))
        self.about_btn.setStyleSheet("QPushButton {\n"
"font: 16pt \"Chiller\";\n"
"color: rgb(255, 255, 255);\n"
"border-radius:30px;\n"
"background-color:rgb(196, 181, 128);\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"background-color: rgb(218, 201, 143);\n"
"}")
        self.about_btn.setObjectName("about_btn")
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setGeometry(QtCore.QRect(192, 102, 361, 255))
        self.widget.setStyleSheet("QLineEdit {\n"
"font: 75 14pt \"Adobe Devanagari\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight:600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"border: None;\n"
"border-bottom:1px solid white;\n"
"}\n"
"\n"
"QLineEdit:hover {\n"
"border-bottom-color: rgb(85, 170, 255);\n"
"}\n"
"\n"
"QWidget {\n"
"background-color: rgba(126, 126, 126, 120);\n"
"background-image: url(:/signup/image/nono);\n"
"}")
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setHorizontalSpacing(10)
        self.formLayout.setVerticalSpacing(30)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setMinimumSize(QtCore.QSize(35, 35))
        self.label.setMaximumSize(QtCore.QSize(16777215, 35))
        self.label.setStyleSheet("font: 14pt \"Chiller\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight:600;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.user_name_le = QtWidgets.QLineEdit(self.widget)
        self.user_name_le.setMinimumSize(QtCore.QSize(35, 35))
        self.user_name_le.setMaximumSize(QtCore.QSize(16777215, 35))
        self.user_name_le.setStyleSheet("QLineEdit {\n"
"font: 75 14pt \"Adobe Devanagari\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight:600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"border: None;\n"
"border-bottom:1px solid white;\n"
"}\n"
"\n"
"QLineEdit:hover {\n"
"border-bottom-color: rgb(85, 170, 255);\n"
"}\n"
"")
        self.user_name_le.setObjectName("user_name_le")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.user_name_le)
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setMinimumSize(QtCore.QSize(35, 35))
        self.label_2.setMaximumSize(QtCore.QSize(16777215, 35))
        self.label_2.setStyleSheet("font: 14pt \"Chiller\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight:600;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.pwd_le = QtWidgets.QLineEdit(self.widget)
        self.pwd_le.setMinimumSize(QtCore.QSize(35, 35))
        self.pwd_le.setMaximumSize(QtCore.QSize(16777215, 35))
        self.pwd_le.setStyleSheet("QLineEdit {\n"
"font: 75 14pt \"Adobe Devanagari\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight:600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"border: None;\n"
"border-bottom:1px solid white;\n"
"}\n"
"\n"
"QLineEdit:hover {\n"
"border-bottom-color: rgb(85, 170, 255);\n"
"}\n"
"")
        self.pwd_le.setEchoMode(QtWidgets.QLineEdit.PasswordEchoOnEdit)
        self.pwd_le.setObjectName("pwd_le")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.pwd_le)
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setMinimumSize(QtCore.QSize(35, 35))
        self.label_3.setMaximumSize(QtCore.QSize(16777215, 35))
        self.label_3.setStyleSheet("font: 14pt \"Chiller\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight:600;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.comfirm_pwd_le = QtWidgets.QLineEdit(self.widget)
        self.comfirm_pwd_le.setMinimumSize(QtCore.QSize(35, 35))
        self.comfirm_pwd_le.setMaximumSize(QtCore.QSize(16777215, 35))
        self.comfirm_pwd_le.setStyleSheet("QLineEdit {\n"
"font: 75 14pt \"Adobe Devanagari\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight:600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"border: None;\n"
"border-bottom:1px solid white;\n"
"}\n"
"\n"
"QLineEdit:hover {\n"
"border-bottom-color: rgb(85, 170, 255);\n"
"}\n"
"")
        self.comfirm_pwd_le.setEchoMode(QtWidgets.QLineEdit.PasswordEchoOnEdit)
        self.comfirm_pwd_le.setObjectName("comfirm_pwd_le")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.comfirm_pwd_le)
        self.signup_btn = QtWidgets.QPushButton(self.widget)
        self.signup_btn.setEnabled(False)
        self.signup_btn.setMinimumSize(QtCore.QSize(0, 40))
        self.signup_btn.setMaximumSize(QtCore.QSize(16777215, 40))
        self.signup_btn.setStyleSheet("QPushButton{\n"
"border-radius: 20px;\n"
"background-color:rgb(207, 39, 39);\n"
"font: 18pt \"Chiller\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight: 700\n"
"}\n"
"QPushButton:hover {\n"
"background-color: rgb(207, 65, 65);\n"
"}\n"
"QPushButton:pressed {\n"
"background-color:rgb(200, 23, 23);\n"
"}\n"
"QPushButton:disabled {\n"
"background-color: rgb(126, 126, 126);\n"
"}")
        self.signup_btn.setObjectName("signup_btn")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.SpanningRole, self.signup_btn)
        self.verticalLayout.addLayout(self.formLayout)
        self.exit_btn.raise_()
        self.reset_btn.raise_()
        self.about_btn.raise_()
        self.widget.raise_()
        self.menu_btn.raise_()

        self.retranslateUi(Form)
        self.signup_btn.clicked.connect(Form.signup_clicked)
        self.user_name_le.editingFinished.connect(Form.format_check)
        self.about_btn.clicked.connect(Form.about_clicked)
        self.reset_btn.clicked.connect(Form.reset_clicked)
        self.exit_btn.clicked.connect(Form.exit_clicked)
        self.comfirm_pwd_le.textChanged['QString'].connect(Form.format_check)
        self.pwd_le.textChanged['QString'].connect(Form.format_check)
        self.menu_btn.toggled['bool'].connect(Form.menu_toggled)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Sign Up"))
        self.menu_btn.setText(_translate("Form", "Menu"))
        self.exit_btn.setText(_translate("Form", "Exit"))
        self.reset_btn.setText(_translate("Form", "Reset"))
        self.about_btn.setText(_translate("Form", "About"))
        self.label.setText(_translate("Form", "User Name"))
        self.label_2.setText(_translate("Form", "Password"))
        self.label_3.setText(_translate("Form", "Comfirm Passowrd"))
        self.signup_btn.setText(_translate("Form", "SIGN UP"))



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
