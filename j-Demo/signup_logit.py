from resource.signup_ui import Ui_Form
from PyQt5.Qt import QWidget,pyqtSignal,Qt,QSequentialAnimationGroup,QPropertyAnimation,QAbstractAnimation,QEasingCurve
import resource.img_rc

class Sign_Panel(QWidget, Ui_Form):

    signup_signal = pyqtSignal(str,str)
    exit_signal = pyqtSignal()

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.setAttribute(Qt.WA_StyledBackground, True)
        # self.setWindowFlags(Qt.FramelessWindowHint)
        self.setupUi(self)
        self.widget.setContentsMargins(10, 10, 10, 10)
        self.btn_pos_group = [self.about_btn.pos(), self.reset_btn.pos(), self.exit_btn.pos()]

    def signup_clicked(self):

        user_name = self.user_name_le.text()
        pwd = self.pwd_le.text()
        self.signup_signal.emit(user_name, pwd)

    def format_check(self):

        user_name = self.user_name_le.text()
        pwd = self.pwd_le.text()
        comfirm_pwd = self.comfirm_pwd_le.text()
        if len(user_name) > 0 and len(pwd) > 0 and len(comfirm_pwd) > 0 and pwd == comfirm_pwd:
            self.signup_btn.setEnabled(True)
        else:
            self.signup_btn.setEnabled(False)

    def about_clicked(self):

        msg = QMessageBox(QMessageBox.Information, "About Me", "<h5>Done by HQL, copy WSZ</h5>", parent=self)
        msg.setWindowModality(Qt.NonModal)
        msg.show()

    def reset_clicked(self):

        self.user_name_le.clear()
        self.pwd_le.clear()
        self.comfirm_pwd_le.clear()

    def exit_clicked(self):
        self.exit_signal.emit()

    def menu_toggled(self, flag):

        seq_animation = QSequentialAnimationGroup(self)

        btn_group = [self.about_btn, self.reset_btn, self.exit_btn]

        print("menu btn clicked",flag)
        for i,btn in enumerate(btn_group):
            ani = QPropertyAnimation(self)
            ani.setTargetObject(btn)
            ani.setPropertyName(b"pos")

            if flag:
                ani.setStartValue(self.btn_pos_group[i])
                ani.setEndValue(self.menu_btn.pos())
                ani.setEasingCurve(QEasingCurve.InBounce)
            else:
                ani.setStartValue(self.menu_btn.pos())
                ani.setEndValue(self.btn_pos_group[i])
                ani.setEasingCurve(QEasingCurve.OutBounce)
            ani.setDuration(200)
            seq_animation.addAnimation(ani)

        seq_animation.start(QAbstractAnimation.DeleteWhenStopped)

