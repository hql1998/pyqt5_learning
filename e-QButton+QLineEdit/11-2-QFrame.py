from PyQt5.Qt import *
import sys


def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)


app = QApplication(sys.argv)

window = QWidget()
window_title = "QFrame"
window.setObjectName("main window: ")
initial(window,window_title, 1200, 900)

frame = QFrame(window)
frame.resize(300, 300)
frame.move(10,10)
frame.setFrameStyle(QFrame.Panel | QFrame.Raised)
frame.setLineWidth(2)
frame.setMidLineWidth(3)
frame.setStyleSheet("background-color:gray")
frame.setFrameRect(QRect(10,10,280,280))


te = QTextEdit("i am a good guy",window)
te.resize(300,300)
te.move(10,320)
te.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
te.setFrameStyle(QFrame.Panel | QFrame.Raised)
te.setLineWidth(2)
te.setMidLineWidth(3)
te.setStyleSheet("background-color:white")
te.setText("你好呀")
# te.setPlainText("你好吗")
# te.setFrameRect(QRect(10,10, 280, 280))
te.setHtml("<h1>are you ok?</h1>")


le = QLineEdit("i am fine, i'll up or down for it",window)
le.setFixedSize(QSize(300,30))
le.move(10,630)
le.setCursorPosition(0)
le.setFocus(True)
# le.setSelection(0,10)
le.cursorForward(True,2)
le.cursorWordForward(True)
le.cursorWordForward(True)

window.show()

sys.exit(app.exec_())