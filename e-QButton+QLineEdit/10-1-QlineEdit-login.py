from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *
from datetime import datetime

class AccountCheck:
    UN_ERROR = 1
    PWD_ERROR = 2
    SUCCESS = 3

    @staticmethod
    def LoginCheck(userName, pwd):
        if userName != "hql":
            return AccountCheck.UN_ERROR
        if pwd != "1998":
            return AccountCheck.PWD_ERROR

        return AccountCheck.SUCCESS


class numberVlidator(QValidator):

    def validate(self, inp_str, pos):
        # Acceptable = 2
        # Intermediate = 1
        # Invalid = 0
        if inp_str.isnumeric():
            inp_num = int(inp_str)
            if 2000 <= inp_num <= int(datetime.now().year):
                return (QValidator.Acceptable,inp_str,pos)
            else:
                return (QValidator.Intermediate,inp_str,pos)
        return (QValidator.Invalid,inp_str,pos)

    def fixup(self, string):
        if int(string) > int(datetime.now().year):
            return str(datetime.now().year)


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)

    def cover_pwd(self,e):
        # print(self.pwd_le.text())
        # print(e)
        pass

    def logEvent(self):
        user_name = self.un_le.text()
        pwd = self.pwd_le.text()

        status = AccountCheck.LoginCheck(user_name, pwd)

        if status == AccountCheck.UN_ERROR:
            self.un_le.setText("")
            self.pwd_le.setText("")
            self.un_le.setFocus()
            self.label.setText("user name incorrect!")
            self.label.adjustSize()
            return None

        if status == AccountCheck.PWD_ERROR:
            self.pwd_le.setText("")
            self.pwd_le.setFocus()
            self.label.setText("password incorrect!")
            self.label.adjustSize()
            return None

        if status == AccountCheck.SUCCESS:
            self.label.setText("login succesfully!")
            self.label.adjustSize()
            return None

    def password_toggle(self):
        action = self.pwd_le.actions()[0]

        if self.pwd_le.echoMode() == QLineEdit.Normal:
            self.pwd_le.setEchoMode(QLineEdit.Password)
            action.setIcon(QIcon(r"eye_close.png"))
        elif self.pwd_le.echoMode() == QLineEdit.Password:
            self.pwd_le.setEchoMode(QLineEdit.Normal)
            action.setIcon(QIcon(r"eye_open.png"))

    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        self.label = QLabel(self)
        self.label.setMinimumWidth(40)

        self.un_le = QLineEdit(self)
        self.un_le.resize(200,30)
        self.un_le.setClearButtonEnabled(True)
        completer = QCompleter(["HQL","hql","中文","zhh","zn"],self)
        self.un_le.setCompleter(completer)

        self.pwd_le = QLineEdit(self)
        self.pwd_le.resize(200, 30)
        self.pwd_le.setEchoMode(QLineEdit.Password)
        self.pwd_le.textChanged.connect(self.cover_pwd)
        # self.pwd_le.setInputMask("9")
        self.pwd_le.setPlaceholderText("input the password!")


        action = QAction(QIcon(r"eye_close.png"),"",self.pwd_le)
        action.triggered.connect(self.password_toggle)
        self.pwd_le.addAction(action, QLineEdit.TrailingPosition)

        validator = numberVlidator()
        self.pwd_le.setValidator(validator)


        self.login_btn = QPushButton(self)
        self.login_btn.setText("Log in")
        self.login_btn.clicked.connect(self.logEvent)

        return None

    def resizeEvent(self, e):
        win_width = e.size().width()
        win_height = e.size().height()
        self.un_le.move((win_width-self.un_le.width())/2, (win_height-self.un_le.height())/2-50)
        self.pwd_le.move((win_width - self.pwd_le.width()) / 2, (win_height-self.pwd_le.height())/2)
        self.label.move((win_width - self.label.width()) / 2, (win_height - self.label.height()) / 2 + 50)
        self.login_btn.move((win_width - self.login_btn.width()) / 2, (win_height - self.login_btn.height()) / 2 + 100)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())