from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        pte = QPlainTextEdit(self)
        pte.insertPlainText("classification")
        pte.resize(300, 300)
        pte.move((iniWindowWidth - pte.width()) / 2, (iniWindowHeight - pte.height()) / 2)
        self.pte = pte

        pte.setPlaceholderText("please input something")

        btn = QPushButton(self)
        self.btn = btn
        btn.setText("Test Button")
        btn.move((iniWindowWidth - btn.width()) / 2, self.pte.y() + self.pte.height() + 20)
        btn.clicked.connect(self.btn_call)

        return None


    def btn_call(self):
        tcf = QTextCharFormat()
        tcf.setFontStrikeOut(True)
        tcf.setUnderlineStyle(QTextCharFormat.DashDotDotLine)
        tcf.setBackground(QColor(210,230,230))
        tcf.setFontPointSize(26)
        tcf.setFontFamily("颜真卿颜体")

        self.pte.setCurrentCharFormat(tcf)
        self.pte.setFocus()



if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())