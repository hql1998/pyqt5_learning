from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class MyQAbsSpinBox(QAbstractSpinBox):

    def stepEnabled(self):
        if not self.text().isnumeric():
            return QAbstractSpinBox.StepNone
        else:
            return QAbstractSpinBox.StepUpEnabled | QAbstractSpinBox.StepDownEnabled

    def stepBy(self, steps):

        num = int(self.text()) + steps
        if num >= 0:
            self.lineEdit().setText(str(num))
        else:
            self.lineEdit().setText(str(0))

    def validate(self, input, pos):

        if self.text().isnumeric():
            return (QValidator.Acceptable, input, pos)
        else:
            return (QValidator.Invalid, input, pos)

    def fixup(self):
        self.lineEdit().setText("0000")



class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        asb = MyQAbsSpinBox(self)
        asb.lineEdit().setText("20")
        asb.resize(100, 50)
        asb.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())