from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class MyTextEdit(QTextEdit):

    def mousePressEvent(self, e):
        url = self.anchorAt(e.pos())
        print(url)
        if len(url) > 0:
            print(url)
            QDesktopServices.openUrl(QUrl(url))
        return super().mousePressEvent(e)

class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        te = MyTextEdit(self)
        self.te = te
        self.tc = self.te.textCursor()
        te.resize(500,500)
        te.move((iniWindowWidth - te.width())/2, (iniWindowHeight - te.height())/2)

        btn = QPushButton(self)
        self.btn = btn
        btn.setText("Test Button")
        btn.move((iniWindowWidth - btn.width())/2, self.te.y() + self.te.height() + 20)
        btn.clicked.connect(self.btn_call)

        self.te.insertHtml("nihao " * 20000 + "<a name='hql' href='https://mi-12.github.io/Radiomics-Author-Network.github.io/'>hql</a>" + "123" * 2000)

        return None

    def btn_call(self):
        # ****************testing api********************
        # self.add_table()
        # self.add_frame()
        # self.auto_formatting()
        # self.word_wrapping()
        # self.set_overwrite()

        self.and_ancher()

        # ****************testing api********************
        return None

    def add_table(self):
        cols = 5
        rows = 15
        ttf = QTextTableFormat()
        ttf.setCellPadding(2)
        ttf.setCellSpacing(2)
        ttf.setAlignment(Qt.AlignCenter)
        col_widths = []
        for i in range(cols):
            col_widths.append(QTextLength(QTextLength.PercentageLength,100/cols))
        ttf.setColumnWidthConstraints(col_widths)
        self.tc.insertTable(rows, cols, ttf)

        self.tc.movePosition(QTextCursor.End)
        self.tc.insertBlock()


        # for i in range(tt.rows()):
        #     for j in range(tt.columns()):
        #         tt.cellAt(i, j)
        # QTextTable
        # QTextTableCell

        return None

    def add_frame(self):

        tff = QTextFrameFormat()
        tff.setBorder(2)
        tff.setBorderStyle(QTextFrameFormat.BorderStyle_Dashed)
        self.tc.insertFrame(tff)

        self.te.setFocus()
        self.tc.movePosition(QTextCursor.End)
        self.tc.insertBlock()

    def auto_formatting(self):

        self.te.setAutoFormatting(QTextEdit.AutoBulletList)

    def word_wrapping(self):
        self.te.setLineWrapMode(QTextEdit.FixedColumnWidth)
        self.te.setLineWrapColumnOrWidth(30)

        self.te.setWordWrapMode(QTextOption.WordWrap)
        return None

    def set_overwrite(self):
        self.te.setOverwriteMode(True)
        self.te.setCursorWidth(20)

    def and_ancher(self):
        self.te.scrollToAnchor("hql")



if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle = "QTextEdit")
    window.show()
    sys.exit(app.exec_())