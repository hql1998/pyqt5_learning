from PyQt5.Qt import *
import sys


def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)


app = QApplication(sys.argv)

window = QWidget()
window_title = "QAbstractButton"
window.setObjectName("main window: ")
initial(window,window_title, 500, 500)

# ****************how to use a abstract class********************

# class MyButton(QAbstractButton):
#
#     def paintEvent(self, e):
#         QPaintEvent
#         painter = QPainter(self)
#         painter.setPen(QPen(QColor("teal"), 4))
#         painter.drawText(QRect(self.rect()), Qt.AlignCenter, self.text())
#         painter.drawRect(QRect(self.rect()))
#
# a_button = MyButton(window)
# a_button.setText("MyButton")
# a_button.move(200,200)
# # a_button.resize(50,50)

# ****************how to use a abstract class********************



# ****************button icons********************

# btn = QPushButton(window)
# btn.move(200,200)
# btn.setText("PushButton")
# btn.setIcon(QIcon(r"E:\360极速浏览器下载\cursor.png"))

# ****************button icons********************



# ****************set shortcuts********************

# btn1 = QPushButton(window)
# btn1.setText("&a_btn")
# btn1.move(200,200)
# btn1.clicked.connect(lambda x: print("btn1 clicked"))
#
# btn2 = QPushButton(window)
# btn2.setText("&b_btn")
# btn2.move(200,240)
# btn2.clicked.connect(lambda x: print("btn2 clicked"))
#
# btn1.setShortcut("Ctrl+A")
# btn2.setShortcut("Ctrl+B")

# ****************set shortcuts********************



# ****************auto-repeat********************

# btn = QPushButton(window)
# btn.setAutoRepeatDelay(2000)
# btn.setAutoRepeat(True)
# btn.setAutoRepeatInterval(1000)
# btn.clicked.connect(lambda x: print("btn.clicked",x))

# ****************auto-repeat********************



# ****************custom click area********************

class MyButton(QPushButton):
    def hitButton(self, e):
        threhold = self.width() * 0.5
        print(e.x(), threhold)
        if e.x() >= threhold:
            return True
        return False

btn = MyButton(window)
btn.setText("button")
btn.move(200,200)
# release and press signal dosen't work for custom click area
# btn.released.connect(lambda x: print("btn clicked"))
btn.clicked.connect(lambda x: print("btn clicked"))

# ****************custom click area********************

window.show()

sys.exit(app.exec_())