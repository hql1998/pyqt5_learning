from PyQt5.Qt import *
import sys


def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)


app = QApplication(sys.argv)

window = QWidget()
window_title = "QToolButton"
window.setObjectName("main window: ")
initial(window,window_title, 500, 500)


# ****************QToolButton********************
btn = QToolButton(window)
btn.setIcon(QIcon(r"E:\360极速浏览器下载\cursor.png"))
btn.setIconSize(QSize(90, 90))

btn.setAutoRaise(True)
btn.setToolTip("well this is a tool button")

menu = QMenu(btn)
btn.setMenu(menu)

sub_menu = QMenu("sub_menu",menu)
act = QAction(QIcon(r"../d_QWidget/Maximize.png"), "action1", menu)
menu.addMenu(sub_menu)
menu.addSeparator()
menu.addAction(act)

btn.setArrowType(Qt.DownArrow)
btn.setPopupMode(QToolButton.MenuButtonPopup)
# ****************QToolButton********************


# ****************buttonGroup********************

bg1 = QButtonGroup(window)
bg2 = QButtonGroup(window)

rb1 = QRadioButton("male",window) #QIcon(r"../d_QWidget/Maximize.png"),
rb2 = QRadioButton("female",window) #QIcon(r"../d_QWidget/Minimize.png"),

rb1.setChecked(True)

rb1.move(200,200)
rb2.move(260,200)

bg1.addButton(rb1,1)
bg1.addButton(rb2,2)

rb3 = QRadioButton("good",window)
rb4 = QRadioButton("bad",window)

rb3.setChecked(True)

rb3.move(200,180)
rb4.move(260,180)

bg2.addButton(rb3,3)
bg2.addButton(rb4,4)

print("bg1.checkedId():",bg1.checkedId())
print("bg2.checkedId():",bg2.checkedId())

# bg1.buttonToggled[int].connect(lambda i,b: print("id:",i, "status:",b))
bg1.buttonClicked[int].connect(lambda i: print("id:",i))

# ****************buttonGroup********************

window.show()

sys.exit(app.exec_())