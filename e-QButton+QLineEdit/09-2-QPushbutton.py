from PyQt5.Qt import *
import sys


def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)


app = QApplication(sys.argv)

window = QWidget()
window_title = "QPushButton"
window.setObjectName("main window: ")
initial(window,window_title, 500, 500)

# ****************set menu button********************
btn = QPushButton(window)
btn.setText("menu")
btn.move(200,200)
menu = QMenu(btn)

new = QAction("new_file",menu)
new.setCheckable(True)
new.setChecked(True)
new.triggered.connect(lambda x: print("new button ",x))

open_a = QAction("open_file",menu)
sub_menu = QMenu("recent_open",menu)
a_ins = QAction("a_file",sub_menu)
sub_menu.addAction(a_ins)

menu.addAction(new)
menu.addAction(open_a)
menu.addSeparator()
menu.addMenu(sub_menu)

btn.setMenu(menu)


window.show()
# ****************set menu button********************



# ****************add right click menu********************
class Window(QWidget):
    def contextMenuEvent(self, evt):
        QContextMenuEvent
        menu = QMenu(self)

        new = QAction("new_file", menu)
        new.setCheckable(True)
        new.setChecked(True)
        new.triggered.connect(lambda x: print("new button ", x))

        open_a = QAction("open_file", menu)
        sub_menu = QMenu("recent_open", menu)
        a_ins = QAction("a_file", sub_menu)
        sub_menu.addAction(a_ins)

        menu.addAction(new)
        menu.addAction(open_a)
        menu.addSeparator()
        menu.addMenu(sub_menu)

        menu.exec_(evt.globalPos())


win2 = Window()
win2.setWindowTitle("win2")
win2.show()
# ****************add right click menu********************


# ****************add right click menu ********************
def customMenu(p):
    menu = QMenu(win3)

    new = QAction("new_file", menu)
    new.setCheckable(True)
    new.setChecked(True)
    new.triggered.connect(lambda x: print("new button ", x))

    open_a = QAction("open_file", menu)
    sub_menu = QMenu("recent_open", menu)
    a_ins = QAction("a_file", sub_menu)
    sub_menu.addAction(a_ins)

    menu.addAction(new)
    menu.addAction(open_a)
    menu.addSeparator()
    menu.addMenu(sub_menu)

    golbal_pos = win3.mapToGlobal(p)

    menu.exec_(golbal_pos)

win3 = QWidget()
win3.setWindowTitle("win3")
win3.setContextMenuPolicy(Qt.CustomContextMenu)
win3.customContextMenuRequested.connect(customMenu)
win3.resize(500,500)

command_btn = QCommandLinkButton("title","description",win3)
command_btn.setDescription(command_btn.description() + " added description")

win3.show()
# ****************add right click menu ********************


sys.exit(app.exec_())