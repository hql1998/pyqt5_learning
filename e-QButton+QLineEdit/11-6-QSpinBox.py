from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class MyQDoubleSpinBox(QDoubleSpinBox):

    def textFromValue(self, v):
        print(v)
        return str(round(v, 3)) + " are you ok? " + str(round(v, 3))


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        sb = QSpinBox(self)
        # sb.resize(100, 20)
        sb.move((iniWindowWidth - 100 * 2) / 2, (iniWindowHeight - 30) / 2)
        sb.setSuffix("年")
        sb.setRange(1900, 2020)
        sb.cleanText()
        sb.setValue(1912.0)
        sb.lineEdit().setPlaceholderText("years")

        sb1 = QSpinBox(self)
        sb1.move((iniWindowWidth - 60) / 2, (iniWindowHeight - 30) / 2)
        sb1.setSuffix("月")
        sb1.setRange(1, 12)
        sb1.lineEdit().setPlaceholderText("months")

        sb2 = QSpinBox(self)
        sb2.move((iniWindowWidth + 60) / 2, (iniWindowHeight - 30) / 2)
        sb2.setSuffix("日")
        sb2.setRange(1, 31)
        sb2.lineEdit().setPlaceholderText("months")

        db = MyQDoubleSpinBox(self)
        db.move((iniWindowWidth - 60) / 2, (iniWindowHeight - 30) / 2 + 50)
        db.setSuffix(" Ratio")
        db.setRange(0.00, 2.00)
        db.setDecimals(3)
        db.setSingleStep(0.20)



        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())