from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = QLabel(self)
        label.setText("CHUNK ONE")
        label.resize(100, 100)
        label.setStyleSheet("background-color:red;color:white;font-weight:900;padding-left:15%")

        label_two = QLabel(self)
        label_two.setText("CHUNK TWO")
        label_two.resize(100, 100)
        label_two.move(100, 100)
        label_two.setStyleSheet("background-color:orange;color:white;font-weight:900;padding-left:15%")


        ani_one = QPropertyAnimation(label,b"pos",self)
        ani_two = QPropertyAnimation(label_two,b"pos",self)

        ani_one.setKeyValues([(0.25,QPoint(iniWindowWidth - 100,0)),
                              (0.50,QPoint(iniWindowWidth - 100,iniWindowHeight-100)),
                              (0.75, QPoint(0, iniWindowHeight - 100)),
                              (1, QPoint(0, 0))])

        ani_two.setKeyValues([(0.25, QPoint(100, iniWindowHeight - 200)),
                              (0.50, QPoint(iniWindowWidth - 200, iniWindowHeight - 200)),
                              (0.75, QPoint(iniWindowWidth - 200, 100)),
                              (1, QPoint(100, 100))])

        ani_one.setDuration(4000)
        ani_two.setDuration(4000)

        ani_one.setEasingCurve(QEasingCurve.InBounce)
        ani_two.setEasingCurve(QEasingCurve.OutBounce)

        ani_squ = QSequentialAnimationGroup(self)
        ani_squ.addAnimation(ani_one)
        ani_squ.addAnimation(ani_two)

        ani_squ.setLoopCount(3)

        ani_squ.start()

        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())