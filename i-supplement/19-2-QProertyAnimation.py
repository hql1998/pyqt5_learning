from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = QLabel(self)
        label.setText("classification")
        label.resize(100, 50)
        label.setStyleSheet("background-color:orange;")
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        animation = QPropertyAnimation(self)
        animation.setTargetObject(label)
        animation.setPropertyName(b"size")

        animation.setStartValue(QSize(0, 0))#QPoint(200,200)

        animation.setEndValue(QSize(100, 50))#QPoint(0, 0)

        animation.setDuration(2000)
        animation.setLoopCount(3)
        animation.setDirection(QAbstractAnimation.Forward)

        animation.setEasingCurve(QEasingCurve.OutBounce)

        animation.start()



        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())