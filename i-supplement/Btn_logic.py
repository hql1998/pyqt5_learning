from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *
from Btn_ui import Ui_Form

class Window(QWidget, Ui_Form):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
    def deal_rightClick(self):
        print("right clicked")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())