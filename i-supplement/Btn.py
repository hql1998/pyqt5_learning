from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Btn(QPushButton):

    rightClicked = pyqtSignal([str], [str, int])

    def mousePressEvent(self, e):
        super().mousePressEvent(e)

        if e.button() == Qt.RightButton:
            self.rightClicked[str].emit("str argument")
            self.rightClicked[str, int].emit("str argument", 127)


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = Btn(self)
        label.setText("classification")
        label.resize(100, 50)
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        label.rightClicked[str, int].connect(lambda st, inte: print("good",st, inte))

        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())