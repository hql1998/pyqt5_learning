from PyQt5.Qt import *
import re

# this file used recursive way to print a tree graph of the inheritance of classes


def print_classes(cls,regex,format = ""):

    cls_str = str(cls)
    re_result = regex.search(cls_str)
    print(format+re_result.group(1))

    if len(cls.__subclasses__()) > 0:
        for i in cls.__subclasses__():
            print_classes(i, regex, format+"-- ")
    else:
        return

# making the regex is to get rid of useless string from < class 'QWidget.QAbstractButton'>
regex = re.compile(r"\..*?\.(.*)\'")

print_classes(QWidget,regex)

# the output is like :

# QWidget
# -- QAbstractButton
# -- -- QCheckBox
# -- -- QPushButton
# -- -- -- QCommandLinkButton
# -- -- QRadioButton
# -- -- QToolButton
# -- QFrame
# -- -- QAbstractScrollArea
# -- -- -- QAbstractItemView
# -- -- -- -- QColumnView
# -- -- -- -- QHeaderView
# -- -- -- -- QListView
# -- -- -- -- -- QListWidget
# -- -- -- -- -- QUndoView
# -- -- -- -- -- QHelpIndexWidget
# -- -- -- -- QTableView
# -- -- -- -- -- QTableWidget
# -- -- -- -- QTreeView
# -- -- -- -- -- QTreeWidget
# -- -- -- -- -- QHelpContentWidget
# -- -- -- QGraphicsView
# -- -- -- QMdiArea
# -- -- -- QPlainTextEdit
# -- -- -- QScrollArea
# -- -- -- QTextEdit
# -- -- -- -- QTextBrowser
# -- -- QLCDNumber
# -- -- QLabel
# -- -- QSplitter
# -- -- QStackedWidget
# -- -- QToolBox
# -- QAbstractSlider
# -- -- QDial
# -- -- QScrollBar
# -- -- QSlider
# -- QAbstractSpinBox
# -- -- QDateTimeEdit
# -- -- -- QDateEdit
# -- -- -- QTimeEdit
# -- -- QDoubleSpinBox
# -- -- QSpinBox
# -- QCalendarWidget
# -- QDialog
# -- -- QColorDialog
# -- -- QErrorMessage
# -- -- QFileDialog
# -- -- QFontDialog
# -- -- QInputDialog
# -- -- QMessageBox
# -- -- QProgressDialog
# -- -- QWizard
# -- -- QAbstractPrintDialog
# -- -- -- QPrintDialog
# -- -- QPageSetupDialog
# -- -- QPrintPreviewDialog
# -- QComboBox
# -- -- QFontComboBox
# -- QDesktopWidget
# -- QDialogButtonBox
# -- QDockWidget
# -- QFocusFrame
# -- QGroupBox
# -- QKeySequenceEdit
# -- QLineEdit
# -- QMainWindow
# -- QMdiSubWindow
# -- QMenu
# -- QMenuBar
# -- QOpenGLWidget
# -- QProgressBar
# -- QRubberBand
# -- QSizeGrip
# -- QSplashScreen
# -- QSplitterHandle
# -- QStatusBar
# -- QTabBar
# -- QTabWidget
# -- QToolBar
# -- QWizardPage
# -- QDesignerActionEditorInterface
# -- QDesignerFormWindowInterface
# -- QDesignerObjectInspectorInterface
# -- QDesignerPropertyEditorInterface
# -- QDesignerWidgetBoxInterface
# -- QHelpSearchQueryWidget
# -- QHelpSearchResultWidget
# -- QVideoWidget
# -- -- QCameraViewfinder
# -- QGLWidget
# -- QPrintPreviewWidget
# -- QSvgWidget
# -- QQuickWidget