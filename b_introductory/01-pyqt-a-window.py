from PyQt5.Qt import *
sys.path.append("..")
from a_constant.constant_appearance import *

# __name__ 变量是一个全局变量，一旦有调用这个py文件，如果是从控制台方式调用，那么name变量中存储的就是"__main__"，如果是从导入的文件，就是该文件名去掉.py
print(__name__)


# 0. 最开始的一步，创建一个应用程序，管理进程。
#    sys.argv 被称作命令行参数，是一个列表，其中第一个成员总是文件名，后面的参数视调用的时候有无实参决定。
#    例如   E:\python\pyQT> python E:\python\pyQT\01-pyqt-a-window.py 第二个参数, 第三个参数, ...
app = QApplication(sys.argv)

# 1. 第二步，创建控件，并定义功能，其中最开始创建的控件是顶层控件，会自动加上最小化，最大化，关闭三个按钮，以及标题栏文字，标题栏图标。
#    QWidget是一个空白的容器控件，只有包括其他控件的作用。
window = QWidget()
window.resize(iniWindowWidth,iniWindowHeight)
window.setMinimumSize(400,300)
window.setWindowTitle(windowTitle)
window.move((screenWidth-iniWindowWidth)/2, (screenHight-iniWindowHeight)/2)

# 1.1在创建好顶层窗口之后，剩下的控件如果需要展示在这个窗口中，那么需要继承这个空间
label = QLabel(window)
label.setText("classification")
label.resize(100,50)
label.move((iniWindowWidth-100)/2,(iniWindowHeight-50)/2)

window.show()
print(app.exec_())
sys.exit()



