import datetime as dt
from PyQt5.Qt import *

# class rect():
#     # the first line below the class name is defined as DocString, which could be reffered by ClassName.__doc__ property
#     """
#     authord: HQL
#     version:0.0
#     """
#     # attributes defined in class is called " class variables "
#     w = 20
#     h = 15
#
#     # functions defined in a class are called "instance method" or "method"
#     # and you need to explicitly write the self
#     def showArea(self):
#         area = w * h
#         print("the area of the rectangle is {0}".format(area))
#
#
# a_rect = rect()
# b_rect = rect()
# print("before class", dir(rect))
# print("the base class(se) is {0}".format(rect.__bases__))
# print(rect.__doc__)
# print("the name of the class is {0}".format(rect.__name__))
# print("the name of the module in which the class is defined: {0}".format(rect.__module__))
# print("before instance",dir(a_rect))
# print("before instance b",dir(b_rect))
# rect.ab = 0
# print("after class",dir(rect))
# print("after instance",dir(a_rect))
# print("after instance b",dir(b_rect))

########################################################

class rect():
    # the first line below the class name is defined as DocString, which could be reffered by ClassName.__doc__ property
    """
    authord: HQL
    version:0.1
    """
    # the line below shows how to define "class attributes"
    dateTime = dt.datetime.now()
    # init method is a built-in method, used to define instance varibles to initiate a specific instance.
    def __init__(self, w=20, h=15):
        self.w = w
        self.h = h
    def __str__(self):
        return "you called __str__ method"
    def __repr__(self):
        return "you called __repr__ method"
    def __add__(self,other):
        return self.w + other

    @classmethod
    def showTime(cls):
        print(cls.dateTime)
        # print(cls.w) # AttributeError: type object 'rect' has no attribute 'w'


    # functions defined in a class are called "instance method" or "method"
    # and you need to explicitly write the self
    def showArea(self):
        area = self.w * self.h
        print("the area of the rectangle is {0}".format(area))
        return area


a_rect = rect(20,15)
b_rect = rect(10,15)
print("before instance",dir(a_rect))
print("before instance b",dir(b_rect))
print(a_rect+1)
a_rect.showArea()
b_rect.showArea()
a_rect.showTime()
b_rect.showTime()

