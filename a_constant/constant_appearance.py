import sys
from PyQt5 import QtWidgets

app = QtWidgets.QApplication(sys.argv)

screen = app.primaryScreen()
size = screen.size()
rect = screen.availableGeometry()

# print('Screen: %s' % screen.name())
# print('Size: %d x %d' % (size.width(), size.height()))
# print('Available: %d x %d' % (rect.width(), rect.height()))

screenHight = rect.height()
screenWidth = rect.width()
iniWindowHeight = 900
iniWindowWidth = 1200

windowTitle = "HQL's classification software"