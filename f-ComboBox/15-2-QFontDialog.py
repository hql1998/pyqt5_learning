from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        label = QLabel(self)
        label.setText("classification")
        label.resize(100, 50)
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        btn = QPushButton(self)
        btn.setText("select font")
        btn.move((iniWindowWidth - btn.width()) / 2, 50)
        def select_font():
            font_label = QFont(label.font())
            font, ok = QFontDialog.getFont(font_label,self,"select a font")#, QFontDialog.MonospacedFonts)
            if ok:
                label.setFont(font)
                label.adjustSize()
        btn.clicked.connect(select_font)

        def select_another_font():
            font_label = QFont(label.font())
            fd = QFontDialog(font_label, self)
            fd.open()
            fd.fontSelected.connect(fd_accepted)


        def fd_accepted(font):
            label.setFont(font)
            label.adjustSize()

        btn1 = QPushButton(self)
        btn1.setText("select another font")
        btn1.move((iniWindowWidth - btn.width()) / 2, 100)

        btn1.clicked.connect(select_another_font)


        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, "QFontDialog")
    window.show()
    sys.exit(app.exec_())