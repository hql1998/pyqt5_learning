from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):

        def open_file():
            open_re = QFileDialog.getOpenFileName(self,"open a file","../",
                                                  "text(*.txt);;source(*.py *.c *.cpp *.html *.css *.js)", "text(*.txt)")
            file_path = open_re[0]
            if len(file_path) >0 :
                with open( file_path, 'r', encoding ="UTF - 8") as f:
                    te.setText(f.read())
                label.setText(file_path.split("/")[-1])
                label.adjustSize()

        def save_file():
            save_re = QFileDialog.getSaveFileName(self, "save a file", "../",
                                                  "text(*.txt);;python(*.py)", "text(*.txt)")
            file_path = save_re[0]
            if len(file_path) > 0:
                with open(file_path, 'w', encoding ="UTF - 8") as f:
                    f.write(te.toPlainText())

        te = QTextEdit(self)
        te.resize(500, 500)
        te.move((iniWindowWidth - te.width())/2, (iniWindowHeight - te.height())/2)

        open_btn = QPushButton(self)
        open_btn.setText("Open File")
        open_btn.clicked.connect(open_file)
        open_btn.move((iniWindowWidth - open_btn.width())/2 - 60, (iniWindowHeight + te.height())/2 + 60)

        save_btn = QPushButton(self)
        save_btn.setText("Save File")
        save_btn.clicked.connect(save_file)
        save_btn.move((iniWindowWidth - save_btn.width()) / 2 + 60, (iniWindowHeight + te.height()) / 2 + 60)

        label = QLabel(self)
        label.setText("Nothing")
        label.move((iniWindowWidth - te.width())/2, (iniWindowHeight - te.height())/2 - label.height())

        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, "a text box")
    window.show()
    sys.exit(app.exec_())