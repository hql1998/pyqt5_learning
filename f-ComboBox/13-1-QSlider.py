from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):

        label = QLabel(self)
        label.setText("0")
        label.resize(100, 50)
        label.move((iniWindowWidth - 100) / 2, (iniWindowHeight - 50) / 2)

        sd = QSlider(self)
        sd.move(10, (iniWindowHeight - sd.height()) / 2)
        sd.setMinimumWidth(500)
        sd.valueChanged.connect(lambda val: label.setText(str(val)))



        # change the range
        sd.setMinimum(-100)
        sd.setMaximum(100)

        # set and get current value
        sd.setValue(12)
        print(sd.value())

        # set the step length by page or single step
        sd.setSingleStep(2)
        sd.setPageStep(10)

        # set if the value is corrodent with the slider when moving
        # sd.setTracking(False)

        # turn over the slider big end and small end
        sd.setInvertedAppearance(True)
        sd.setInvertedControls(True)

        # set horizontal direction
        sd.setOrientation(Qt.Horizontal)

        # get if the slider were pressed
        print(sd.isSliderDown())

        # set ticks
        sd.setTickPosition(QSlider.TicksBothSides)


        # add a label to show
        lab_on_sd = QLabel(sd)
        lab_on_sd.setText("0")
        lab_on_sd.setStyleSheet("color:cyan")
        lab_on_sd.hide()


        def press_sd():

            print(sd.isSliderDown())
            lab_on_sd.show()

        def move_sd(val):
            lab_on_sd.setText(str(val))
            lab_on_sd.adjustSize()
            x = (sd.maximum() - sd.minimum() - ( sd.value() - sd.minimum()) ) / (sd.maximum() - sd.minimum()) * (sd.width() - lab_on_sd.width())
            lab_on_sd.move(x, 9)


        move_sd(sd.value())
        sd.sliderPressed.connect(press_sd)
        sd.valueChanged.connect(move_sd)
        sd.sliderReleased.connect(lambda : lab_on_sd.hide())



        qd= QDial(self)
        qd.move((screenWidth - iniWindowWidth) / 2, 10)
        qd.setNotchesVisible(True)
        qd.setRange(0,360)
        qd.setPageStep(10)
        qd.valueChanged.connect(lambda val:print("QDial value:", val))

        # fill the notch gap on tha dial
        qd.setWrapping(True)


        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())