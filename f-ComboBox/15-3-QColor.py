from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        btn0 = QPushButton(self)
        btn0.setText("select a color")
        btn0.move((iniWindowWidth-btn0.width())/2, 50)

        def selected_color(color):
            palette = QPalette()
            palette.setColor(QPalette.Background, color)
            self.setPalette(palette)

        def select_color():
            cd = QColorDialog(self)
            cd.open()
            cd.colorSelected.connect(selected_color)

        btn0.clicked.connect(select_color)

        btn1 = QPushButton(self)
        btn1.setText("select another color")
        font = QFont()
        font.setPointSize(30)
        btn1.setFont(font)
        btn1.adjustSize()
        btn1.move((iniWindowWidth - btn1.width()) / 2, 100)


        def select_another_color():

            color = QColorDialog.getColor(QColor(120,120,30), self, "select another color")
            palette = QPalette()
            palette.setColor(QPalette.ButtonText, color)
            btn1.setPalette(palette)

        btn1.clicked.connect(select_another_color)


        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())