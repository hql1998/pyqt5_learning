from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        fcb = QFontComboBox(self)
        fcb.resize(150,30)
        fcb.move((iniWindowWidth - 200) / 2, (iniWindowHeight - 30) / 2)

        fcb.currentFontChanged.connect(lambda font: fcb.lineEdit().setFont(font))
        fcb.setSizeAdjustPolicy(QComboBox.AdjustToMinimumContentsLengthWithIcon)
        # QComboBox.SizeAdjustPolicy

        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())