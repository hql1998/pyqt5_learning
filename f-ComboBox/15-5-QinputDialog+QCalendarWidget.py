from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        input_d = QInputDialog(self)
        input_d.setInputMode(QInputDialog.TextInput)
        input_d.setOption(QInputDialog.UsePlainTextEditForTextInput)
        input_d.setOption(QInputDialog.UseListViewForComboBoxItems)
        input_d.setComboBoxItems(["a", "b"])
        input_d.setComboBoxEditable(True)
        input_d.open()

        dw = QCalendarWidget(self)
        dw.move(20,20)
        dw.activated.connect(lambda x: print("the selected date is ", x))


        input_d.textValueSelected.connect(lambda str: print(str))


        return None

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())