from PyQt5.Qt import *
import sys


def initial(window,window_title,width,height):
    screen_size = app.primaryScreen().size()
    window_size = QSize(width,height)
    center_point = QPoint((screen_size.width() - window_size.width()) / 2,
                          (screen_size.height() - window_size.height()) / 2)

    window.resize(window_size)
    window.move(center_point)
    window.setWindowTitle(window_title)


app = QApplication(sys.argv)

window = QWidget()
window_title = "QDialog"
window.setObjectName("main window: ")
initial(window,window_title, 500, 500)

d = QDialog(window)
d.setSizeGripEnabled(True)
d.open()
d.setModal(True)
d.resize(400,400)
d.accept()
# d.reject()
# d.done()

window.show()

sys.exit(app.exec_())