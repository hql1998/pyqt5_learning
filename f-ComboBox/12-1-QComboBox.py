from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)



        self.data = {
            "北京": {
                "东城": "001",
                "西城": "002",
                "朝阳": "003",
                "丰台": "004"
            },
            "上海": {
                "黄埔": "005",
                "徐汇": "006",
                "长宁": "007",
                "静安": "008",
                "松江": "009"
            },
            "广东": {
                "广州": "010",
                "深圳": "011",
                "湛江": "012",
                "佛山": "013"
            }
        }

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):
        cb = QComboBox(self)
        cb.resize(200, 30)
        cb.move((iniWindowWidth - 200) / 2, (iniWindowHeight - 30) / 2)

        cb.setEditable(True)
        cb.addItem("classification")
        cb.insertSeparator(1)
        cb.addItems(["hql","lzr","zz","yk"])
        cb.setCurrentIndex(2)

        cb_provence = QComboBox(self)
        self.cb_provence = cb_provence
        cb_provence.resize(200, 30)
        cb_provence.move((iniWindowWidth - 100) / 2 - 120, (iniWindowHeight - 30) / 2 + 60)

        cb_city = QComboBox(self)
        self.cb_city = cb_city
        cb_city.resize(200, 30)
        cb_city.move((iniWindowWidth - 100) / 2 + 120, (iniWindowHeight - 30) / 2 + 60)

        cb_provence.currentIndexChanged[str].connect(self.update_city_cb)
        cb_city.currentIndexChanged.connect(self.trans_city_num)

        cb_provence.addItems(self.data.keys())

        return None

    def update_city_cb(self, pro):
        self.cb_city.blockSignals(True)
        self.cb_city.clear()
        self.cb_city.blockSignals(False)
        for key in self.data[pro].keys():
            self.cb_city.addItem(key, self.data[pro][key])

    def trans_city_num(self, idx):
        print(self.cb_city.itemData(idx))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle)
    window.show()
    sys.exit(app.exec_())