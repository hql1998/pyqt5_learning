from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *


class Window(QWidget):
    def __init__(self, screenWidth, screenHight, iniWindowWidth, iniWindowHeight, windowTitle):
        super().__init__()
        self.resize(iniWindowWidth, iniWindowHeight)
        self.setMinimumSize(400, 300)
        self.setWindowTitle(windowTitle)
        self.move((screenWidth - iniWindowWidth) / 2, (screenHight - iniWindowHeight) / 2)

        self.setup_ui(iniWindowWidth, iniWindowHeight)


    def setup_ui(self, iniWindowWidth, iniWindowHeight):

        self.rb = QRubberBand(QRubberBand.Rectangle,self)

        btn_group = QButtonGroup(self)
        btn_group.setExclusive(False)
        for i in range(30):
            cb = QCheckBox("the " + str(i), self)
            btn_group.addButton(cb)
            cb.move(80*(i%5)+10,80*(i//5)+10)



        return None

    def mousePressEvent(self, evt):

        self.origin_pos = evt.pos()
        self.rb.setGeometry(QRect(self.origin_pos, QSize()))
        self.rb.setVisible(True)

    def mouseMoveEvent(self, evt):
        size = QSize(evt.pos().x() - self.origin_pos.x(),evt.pos().y() - self.origin_pos.y())
        rect = QRect(self.origin_pos, size).normalized()
        self.rb.setGeometry(rect)

    def mouseReleaseEvent(self, evt):
        for child in self.findChildren(QCheckBox):
            if self.rb.geometry().contains(child.geometry()) and child.inherits("QCheckBox"):
                child.toggle()
        self.rb.setVisible(False)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window(screenWidth, screenHight, iniWindowWidth, iniWindowHeight, "QRubberBand")
    window.show()
    sys.exit(app.exec_())