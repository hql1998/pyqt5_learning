from PyQt5.Qt import *
import sys
sys.path.append("..")
from a_constant.constant_appearance import *
from login_ui import Ui_Form

class Window(QWidget, Ui_Form):
    def __init__(self):
        super().__init__()
        self.setupUi(self)


    def login_button(self):
        print("good!")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window()

    window.show()
    sys.exit(app.exec_())